all: performance-by-govt-schools.xhtml \
	 performance-by-subject-medium.csv \
	 performance-by-subject-caste.csv \
	 performance-by-subject-gender.csv

%.xhtml: %.csv %.html
	python '$(VIS)' $+ > $@

performance-by-govt-schools.csv: performance-by-govt-schools.py 2011.csv
	python $+ > $@

performance-by-subject-medium.csv: performance-by-subject.py 2011.csv
	python $+ NRC_MEDIUM > $@

performance-by-subject-caste.csv: performance-by-subject.py 2011.csv
	python $+ NRC_CASTE_CODE > $@

performance-by-subject-gender.csv: performance-by-subject.py 2011.csv
	python $+ NRC_GENDER_CODE > $@
