"""
Get each school's name, average marks and whether it's a govt school or now
"""

import sys
import csv

marks = {}
district = {}

for row in csv.DictReader(open(sys.argv[1])):
    school_name = row['SCHOOL_NAME']
    mark = row['TOTAL_MARKS']

    # Ignore students who didn't write the exam
    if not mark.isdigit(): continue

    # Against each school, store the students marks
    marks.setdefault(school_name, []).append(int(mark))
    district[row['SCHOOL_NAME']] = row['DIST_CODE']

out = csv.writer(sys.stdout, lineterminator='\n')
out.writerow(['District', 'School', 'Average', 'Count', 'Govt?'])
for school_name in marks:
    school_average = float(sum(marks[school_name])) / len(marks[school_name])
    is_govt_school = ('GOVT' in school_name) or ('GOVERNMENT' in school_name)
    out.writerow([district[school_name], school_name.strip(), '%0.3f' % school_average, len(marks[school_name]), 'G' if is_govt_school else 'N'])
