"""
Get the average performance by subject and parameter of instruction
"""

import re
import sys
import csv

marks = {}

mark_pattern = re.compile(r'(\d\d)([A-Z]?)\*?(\d+)')
field = sys.argv[2]

for row in csv.DictReader(open(sys.argv[1])):
    parameter = row[field]

    for subject in 'S1 S2 S3 L1 L2 L3'.split(' '):
        mark = row[subject + '_MARKS']

        # Ignore those who didn't take the exam
        if mark == 'X0' or mark == 'X*0' or mark.endswith('888'): continue

        match = mark_pattern.match(mark)
        key = (parameter, match.group(1))
        marks.setdefault(key, []).append(int(match.group(3)))

out = csv.writer(sys.stdout, lineterminator='\n')
out.writerow([field, 'Subject', 'Average', 'Count'])
for parameter, subject in marks:
    all_marks = marks[parameter, subject]
    out.writerow([ parameter, subject, '%0.3f' % (float(sum(all_marks)) / len(all_marks)), len(all_marks) ])
